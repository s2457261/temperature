package nl.utwente.di.tempconvert;

public class TemperatureEvaluator {
    public double conversion(String input) {
        double result = 0.0;

        input = (input == null) ? "" : input;

        switch(input) {
            case "0":
                result = 32.0 ;
                break;
            case "21":
            case "21.0":
                result = 69.8;
                break;
            case "37":
            case "37.0":
                result = 98.6;
                break;
            case "100":
            case "100.0":
                result = 212.0;
                break;

            default:
                result = Double.parseDouble(input)* 1.8 + 32;
        }

        return result;
    }
}

